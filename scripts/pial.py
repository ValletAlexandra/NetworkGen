import networkx as nx
import pymesh
import numpy as np
from networkgen.generationtools import rectangle_mesh
from networkgen.io import export_to_paraview 

### Creation of an interconnected network of loops : Robust Backbone

# Create a mesh representing the brain surface  

# Here a simple rectangle 10 mm x 10 mm but is could be a 3D brain surface
# We can also use another meshing method from another library in order to get a more regular mesh

p1=[0,0,0]
p2=[0,10,0]
p3=[10,10,0]
p4=[10,0,0]

# typical pial vessel length in mice : 1mm
mesh = rectangle_mesh([p1,p2,p3,p4], res=1)


# Get the dual graph in order to have a network of 3 connected nodes like in pial vessel network
# See : https://www.pnas.org/content/107/28/12670

vertices,edges=pymesh.mesh_to_dual_graph(mesh)

# Convert to a networkx graph 
G=nx.Graph()
G.add_edges_from(edges)

nx.set_node_attributes(G, np.nan, "pos")
for i,v in enumerate(vertices) :
        G.nodes[i]['pos']=v

# Set a fixed value for radius
# fixed value of the radius : 100 um
nx.set_node_attributes(G, 100e-3, "radius")

# Todo : Add penetrating arterioles
# randomly distribute the entrance of penetrating arterioles and connect to closer nodes
# or grow tree with random 1 to 4 generation  in the cells for the penetrating arterioles

# Todo : Add inlet from main vessels
# Should discuss about this : same input at all outer vertex in the case of rectangle ?
# Randomly equaly distributed points in the case of a closed surface ?


# Export for visualisation / computation

filename='/workspaces/NetworkGen/output/test'

export_to_paraview(G, filename)