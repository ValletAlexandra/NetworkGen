import networkx as nx
import pymesh
import numpy as np
import argparse
from numpy.linalg import norm

from networkgen.geometrytools import project_onto_plane, rotate_in_plane, normalize, translation, distance


def compute_vessel_endpoint (previousvessel, surfacenormal,angle,length) :
    """ From a previous vessel defined in 3D, the brain surface at the end of the previous vessel, angle and length : 
        compute the coordinate of the end node of the current vessel """

    # project the previous vessel in the current plane
    # and get the direction vector of the previous vessel 
    pm1=previousvessel[0]
    p0=previousvessel[1]

    vector_previous=[p0[i]-pm1[i] for i in range(len(pm1))]

    previousdir = project_onto_plane(vector_previous, surfacenormal)


    # compute the direction vector of the new vessel with the angle
    newdir=rotate_in_plane(previousdir,surfacenormal,angle)

    # compute the location of the end of the new vessel
    pnew=translation(p0,newdir,length)


    return pnew



def rectangle_mesh(points, res) :
    """ Create a rectangular mesh from a list of 4 points (defined in a loop order) with a given cell length.
    Note : Here the the pymesh library is used to generate the mesh. We loose the rectangular shape at the corners"""

    vertices=np.array(points)

    faces =np.array([
                        [0,1,2],
                        [0,2,3]
                        ])

    mesh = pymesh.form_mesh(vertices, faces)

    # Remesh with a given resolution size
    bbox_min, bbox_max = mesh.bbox
    diag_len = norm(bbox_max - bbox_min)
    
    mesh=refine_mesh(mesh, refinement=res/diag_len)

    return mesh

def refine_mesh(mesh, refinement=5e-2):
    """ Refine the mesh by splitting the edges until the target resolution is reached 
    refinement : ratio of the typical cell size over the whole object size """

    bbox_min, bbox_max = mesh.bbox
    diag_len = norm(bbox_max - bbox_min)
    target_len = diag_len * refinement

    print("Target resolution: {} ".format(target_len))

    count = 0
    mesh, __ = pymesh.remove_degenerated_triangles(mesh, 100)
    mesh, __ = pymesh.split_long_edges(mesh, target_len)
    num_vertices = mesh.num_vertices
    while True:
        #mesh, __ = pymesh.collapse_short_edges(mesh, 1e-6)
        mesh, __ = pymesh.collapse_short_edges(mesh, target_len,
                                               preserve_feature=True)
        mesh, __ = pymesh.remove_obtuse_triangles(mesh, 150.0, 100)
        if mesh.num_vertices == num_vertices:
            break

        num_vertices = mesh.num_vertices
        print("number of vertices: {}".format(num_vertices))
        count += 1
        if count > 10: break

    mesh = pymesh.resolve_self_intersection(mesh)
    mesh, __ = pymesh.remove_duplicated_faces(mesh)
    mesh = pymesh.compute_outer_hull(mesh)
    mesh, __ = pymesh.remove_duplicated_faces(mesh)
    mesh, __ = pymesh.remove_obtuse_triangles(mesh, 179.0, 5)
    mesh, __ = pymesh.remove_isolated_vertices(mesh)

    return mesh




if __name__ == '__main__':

    vertices=np.array([
                        [0,0,0],
                        [0,1,0],
                        [1,1,0],
                        [1,0,0]])
    faces =np.array([
                        [0,1,2],
                        [0,2,3]
                    ])

    mesh = pymesh.form_mesh(vertices, faces)

    mesh=refine_mesh(mesh, detail=5e-2)

    #vertices,edges=pymesh.mesh_to_graph(mesh)
    vertices,edges=pymesh.mesh_to_dual_graph(mesh)

    G=nx.Graph()

    G.add_edges_from(edges)

    nx.set_node_attributes(G, 0.01, "radius")
    nx.set_node_attributes(G, np.nan, "pos")
    for i,v in enumerate(vertices) :
        G.nodes[i]['pos']=v
