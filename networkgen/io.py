import networkx as nx
import vtk


def write_vtk(node_coords=None,
              edges = None,
              scalar = None,
              scalar_name = 's',
              method = 'vtkPolyData',
              fileout = 'test'):
    """
    Store points and/or graphs as vtkPolyData or vtkUnstructuredGrid.
    Required argument:
    - nodeCoords is a list of node coordinates in the format [x,y,z]
    Optional arguments:
    - edges is a list of edges in the format [nodeID1,nodeID2]
    - scalar is the list of scalar for each node
    - scalar_name is the scalar's name
    - method = 'vtkPolyData' or 'vtkUnstructuredGrid'
    - fileout is the output file name (will be given .vtp or .vtu extension)
    """

    points = vtk.vtkPoints()
    for node in node_coords:
        points.InsertNextPoint(node)

    if edges:
        line = vtk.vtkCellArray()
        line.Allocate(len(edges))
        for edge in edges:
            line.InsertNextCell(2)
            line.InsertCellPoint(int(edge[0]))
            line.InsertCellPoint(int(edge[1]))   # line from point edge[0] to point edge[1]

    if scalar:
        attribute = vtk.vtkFloatArray()
        attribute.SetNumberOfComponents(1)
        attribute.SetName(scalar_name)
        attribute.SetNumberOfTuples(len(scalar))

        for i, j in enumerate(scalar):   # i becomes 0,1,2,..., and j runs through scalars
            attribute.SetValue(i,j)

    if method == 'vtkPolyData':
        polydata = vtk.vtkPolyData()
        polydata.SetPoints(points)
        if edges:
            polydata.SetLines(line)
        if scalar:
            polydata.GetCellData().AddArray(attribute)
        writer = vtk.vtkXMLPolyDataWriter()
        writer.SetFileName(fileout+'.vtp')
        writer.SetInputData(polydata)
        writer.Write()
    elif method == 'vtkUnstructuredGrid':
        # caution: ParaView's Tube filter does not work on vtkUnstructuredGrid
        grid = vtk.vtkUnstructuredGrid()
        grid.SetPoints(points)
        if edges:
            grid.SetCells(vtk.VTK_LINE, line)
        if scalar:
            grid.GetPointData().AddArray(attribute)
        writer = vtk.vtkXMLUnstructuredGridWriter()
        writer.SetFileName(fileout+'.vtu')
        writer.SetInputData(grid)
        writer.Write()

def export_to_paraview(network, filename):
    """
    Export a graph to a paraview output format.
    Required argument:
    - network is a graph object from the networkx library
    - fileout is the output file name (will be given .vtp or .vtu extension)
    """
    # renumbering of the nodes
    network = nx.convert_node_labels_to_integers(network)

    nodecoords = [network.nodes[n]['pos'] for n in network.nodes]
    listofedges = [[int(e[0]), int(e[1])] for e in network.edges]
    listofradius = [network.edges[e]['radius'] for e in network.edges]

    write_vtk(node_coords=nodecoords,
              edges=listofedges,
              scalar=listofradius,
              scalar_name='radius',
              method='vtkPolyData',
              fileout=filename)
