from math import sqrt
from scipy.spatial.transform import Rotation
import numpy as np

def distance(p1,p2):
    return np.sqrt(np.sum([(p1[i]-p2[i])**2 for i in range(len(p1)) ]))

def dot_product(x, y):
    return sum([x[i] * y[i] for i in range(len(x))])

def norm(x):
    return sqrt(dot_product(x, x))

def normalize(x):
    return [x[i] / norm(x) for i in range(len(x))]

def project_onto_plane(x, n):
    d = dot_product(x, n) / norm(n)
    p = [d * normalize(n)[i] for i in range(len(n))]
    return [x[i] - p[i] for i in range(len(x))]

def rotate_in_plane(x,n,angle):
    """ angle : rotation degree """

    rotation_radians = np.radians(angle)

    rotation_axis = np.array(n)

    rotation_vector = rotation_radians * rotation_axis

    rotation = Rotation.from_rotvec(rotation_vector)

    rotated_vec = rotation.apply(x)

    return rotated_vec

def translation(p0,direction,length) :
    #normalise the direction vector
    direction=normalize(direction)

    # compute the location of the end of the new vessel
    pnew=[(p0[i]+direction[i]*length) for i in range(len(p0))]
    return pnew